module sisyphus

go 1.12

require (
	cloud.google.com/go v0.41.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	google.golang.org/genproto v0.0.0-20190701230453-710ae3a149df // indirect
	google.golang.org/grpc v1.22.0 // indirect
	gopkg.in/yaml.v2 v2.2.1
	k8s.io/api v0.0.0-20190602125759-c1e9adbde704
	k8s.io/apimachinery v0.0.0-20190602125621-c0632ccbde11
	k8s.io/client-go v0.0.0-20190602130007-e65ca70987a6
)
